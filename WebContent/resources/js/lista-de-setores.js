var inicio = new Vue({
	el:"#inicio",
    data: {
		listaSetores: [],
		setor:'',
       
    },
    created: function(){
        let vm =  this;
		vm.buscaSetores();
		
    },
    methods:{
        buscaSetores: function(){
			const vm = this;
			axios.get("/funcionarios/rs/setor/list")
			.then(response => {vm.listaSetores = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			})
		},
		deleteSetor: function(id) {
			const vm = this;
                axios.delete("/funcionarios/rs/setor/delete/"+id)
                .then(response => {
				  
			
                }).finally(function(){
					vm.buscaSetores();
				})
			},

		updateSetor: function (id) {
			const vm = this;
			axios.put("/funcionarios/rs/setor/update/" + id, vm.setor)
				.then(response => {
					
					
					alert("Setor modificado com Sucesso");
				}).catch(function (error){
					alert("Setor não modificado ");
					
				}).finally(function () {
					vm.buscaSetores();
				})
		},

			pegaSetor: function(setorEscolhido){
				const vm = this;
				vm.setor = setorEscolhido;
			}
    }
});