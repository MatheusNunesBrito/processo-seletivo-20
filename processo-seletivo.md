# Processo Seletivo - 20

Ferramentas utilizadas:
* Eclipse IDE
* TomCat v9.0
* MySql v8.0.23
* Insomnia 

-Com o projeto clonado, fa�a a instala��o do TomCat e MySql.
-Configure o Eclipse para fazer a utiliza��o do servidor TomCat.
-Importar projeto para o Eclipse: Eclipse>server>properties>switch location e por fim selecionar TomCat.
-Abra o arquivo persistence.xml e troque as configura��es para as que foram definidas na instala��o do MySql.
-no MySQL Workbench crie um novo Schema: My SQL connections > localhost. Selecione a op��o "Create a new schema in the connected server" 
''funcionariosdb''
-Depois de criado o banco de dados volte ao Eclipse e execute o projeto no servidor TomCat: Run as> run on server.
-Acessando a aplica��o: Abra a url "localhost:8080/funcionarios/".